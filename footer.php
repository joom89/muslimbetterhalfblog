<?php
/**
 * @package 	WordPress
 * @subpackage 	Blogosphere
 * @version		1.0.2
 * 
 * Website Footer Template
 * Created by CMSMasters
 * 
 */
?>

<div id="mbh-footer">
    <div class="mbh-footer-top">
        <div class="mbh-footer-top-col">
            <h3>COMPANY</h3>
            <ul>
                <li><a href="/jobs">Jobs</a></li>
                <li><a href="/contact">Contact</a></li>
                <li><a href="/contact">Press Kit</a></li>
                <!--<li><a href="#signupModal" data-toggle="modal">Sign up</a></li>-->
                <li><a href="#subscriptionModal" data-toggle="modal">Subscription</a></li>
            </ul>
        </div>
        <div class="mbh-footer-top-col">
            <h3>COMMUNITY</h3>
            <ul>
                <li><a href="/">Blog</a></li>
                <li><a href="/contact">Help & Support</a></li>
            </ul>
        </div>
        <div class="mbh-footer-top-col">
            <h3>LEGAL</h3>
            <ul>
                <li><a href="/privacy-policy">Privacy Policy</a></li>
                <li><a href="/cookie-policy">Cookie Policy</a></li>
                <li><a href="/safety-tips">Safety Tips</a></li>
                <li><a href="/security">Security</a></li>
            </ul>
        </div>
    </div>
    <div class="mbh-footer-center">
            <span class="footer-center-title">DOWNLOAD THE APP</span>
            <a class="footer-center-link ios" target="_blank" href="https://testflight.apple.com/join/aBfivtyo"><img src="<?php echo get_template_directory_uri().'/theme-framework/theme-style/img/app-store.png' ?>" /> </a>
            <a class="footer-center-link google" target="_blank" href="https://play.google.com/store/apps/details?id=com.muslimbetterhalf.android"><img src="<?php echo get_template_directory_uri().'/theme-framework/theme-style/img/icon-google-play.png' ?>" /> </a>
    </div>
    <div class="mbh-footer-bottom">
        <span class="mbh-copy-right" >
            <?php
                echo "Made with Love & Sacred Intentions in Okemos, Michigan <br />
                    © Copyright Dup-Dup, LLC. All Right Reserved Muslim Better Half<br />
                    Dup-Dup, LLC P.O. Box 200 OKEMOS, MI 48805";
            ?>
            <?php //echo "© ".date('Y')." Dup-Dup, LLC., All Right Reserved." ?>

        </span>
        <a class="footer-bottom-link" target="_blank" href="https://vimeo.com/user113768928"><img src="<?php echo get_template_directory_uri().'/theme-framework/theme-style/img/vimeo.png' ?>" /> </a>
        <a class="footer-bottom-link" target="_blank" href="https://pin.it/Yo6aHO3"><img src="<?php echo get_template_directory_uri().'/theme-framework/theme-style/img/pinterest.png' ?>" /> </a>
        <a class="footer-bottom-link" target="_blank" href="https://muslimbetterhalfai.medium.com"><img src="<?php echo get_template_directory_uri().'/theme-framework/theme-style/img/medium.png' ?>" /> </a>
        <a class="footer-bottom-link" target="_blank" href="https://www.youtube.com/channel/UCQ8fqbN53VRWxlgS9gwkbvw"><img src="<?php echo get_template_directory_uri().'/theme-framework/theme-style/img/youtube.png' ?>" /> </a>
        <a class="footer-bottom-link" target="_blank" href="https://www.facebook.com/MuslimbetterHalfAPP"><img src="<?php echo get_template_directory_uri().'/theme-framework/theme-style/img/facebook.png' ?>" /> </a>
        <a class="footer-bottom-link" target="_blank" href="https://www.instagram.com/muslimbetterhalf"><img src="<?php echo get_template_directory_uri().'/theme-framework/theme-style/img/ins.png' ?>" /> </a>
        <a class="footer-bottom-link" target="_blank" href="https://twitter.com/MuslimHalf"><img src="<?php echo get_template_directory_uri().'/theme-framework/theme-style/img/twitter.png' ?>" /> </a>
    </div>
</div>

    <!-- signup Modal -->
    <div class="modal fade signupModal" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="signupModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <div class="modal-body-wrap">
                        <h5 class="modal-title" id="signupModalLabel">Sign up today and you are one step <br> closer to a happy marriage.</h5>
                        <div class="text">
                            <p>Newletter, promotions, stories, and fun in your in-box.</p>
                        </div>
                        <div class="signup-form">
                            <?php //echo do_shortcode('[mc4wp_form id="14311"]'); ?>
                            <!--<div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email address">
                            </div>
                            <button type="submit" class="btn">Sign me up</button>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- subscription Modal -->
    <div class="modal fade subscriptionModal" id="subscriptionModal" tabindex="-1" role="dialog" aria-labelledby="subscriptionModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <div class="modal-body-wrap">
                        <h5 class="modal-title" id="subscriptionModalLabel">Access our blogs</h5>
                        <div class="text">
                            <p>Our blogs have all you want to know about muslim marriage.</p>
                        </div>
                        <div class="subscription-form">
                            <?php echo do_shortcode('[mc4wp_form id="14311"]'); ?>
                            <!--<div class="form-group">
                                <input type="email" class="form-control" placeholder="Email address">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="privacy">
                                <label class="custom-control-label" for="privacy">
                                    By continuing, you agree to <a href="/privacy-policy">MuslimBetterHalf’s Privacy Policy.</a>
                                </label>
                            </div>
                            <button type="submit" class="btn">Enjoy Reading</button>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php wp_footer(); ?>
    <script>
        jQuery(document).ready(function(){
            jQuery('.custom-menu-subscription a').attr('data-toggle','modal');
        });

    </script>

</body>
</html>



<?php
/*
$cmsmasters_option = blogosphere_get_global_options();
?>


		</div>
	</div>
</div>
<!-- Finish Middle -->
<?php 

get_sidebar('bottom');

?>
<a href="<?php echo esc_js("javascript:void(0)"); ?>" id="slide_top" class="cmsmasters_slide_top_icon"><span></span></a>
</div>
<!-- Finish Main -->

<!-- Start Footer -->
<footer id="footer">
	<?php 
	get_template_part('theme-framework/theme-style' . CMSMASTERS_THEME_STYLE . '/template/footer');
	?>
</footer>
<!-- Finish Footer -->

<?php do_action('cmsmasters_after_page', $cmsmasters_option); ?>
</div>
<span class="cmsmasters_responsive_width"></span>
<!-- Finish Page -->

<?php do_action('cmsmasters_after_body', $cmsmasters_option); ?>
<?php wp_footer(); ?>
</body>
</html>
