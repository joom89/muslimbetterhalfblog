<?php 
/**
 * @package 	WordPress
 * @subpackage 	Blogosphere
 * @version		1.0.1
 * 
 * Template Functions
 * Created by CMSMasters
 * 
 */


/* Get Previous & Next Post Links Function */
function blogosphere_prev_next_posts($order_cat = false, $tax_name = 'category') {
	$cmsmasters_post_type = get_post_type();
	
	$published_posts = wp_count_posts($cmsmasters_post_type)->publish;
	
	$prevPost = get_previous_post();
	
	$nextPost = get_next_post();
	
	if ($published_posts > 1) {
		echo '<aside class="post_nav">';
		
			if($prevPost) {
				$prevthumbnail = get_the_post_thumbnail($prevPost->ID, array(90,90));
				
				previous_post_link('%link',
				'<span class="cmsmasters_prev_post">
					<span class="post_nav_thumb_wrap">' . $prevthumbnail . '</span>
					<span class="post_nav_title_wrap"><span class="post_nav_sub">' . esc_html__('Previous', 'blogosphere') . ' ' . $cmsmasters_post_type . '</span><span class="post_nav_title">%title</span></span>
				</span>', $order_cat, $tax_name);
			} 
			
			if($nextPost) { 
				$nextthumbnail = get_the_post_thumbnail($nextPost->ID, array(90,90));
				
				next_post_link('%link',
				'<span class="cmsmasters_next_post">
					<span class="post_nav_thumb_wrap">' . $nextthumbnail . '</span>
					<span class="post_nav_title_wrap"><span class="post_nav_sub">' . esc_html__('Next', 'blogosphere') . ' ' . $cmsmasters_post_type . '</span><span class="post_nav_title">%title</span></span>
				</span>', $order_cat, $tax_name);
			} 
		
		echo '</aside>';
	}
}



/* Get Sharing Box Function */
function blogosphere_sharing_box($title_box = false, $tag = 'h3') {
	if (class_exists('Cmsmasters_Content_Composer')) {
		echo cmsmasters_sharing_box($title_box, $tag);
	}
}

function mbh_social_buttons($atts,$content) {
    $defaults = array(
        'is_sticky' => false,
    );
    $arr = (object) shortcode_atts($defaults, $atts);
    global $post;
    if(is_singular()){

        // Get current page URL
        $sb_url = urlencode(get_permalink());

        // Get current page title
        $sb_title = str_replace( ' ', '%20', get_the_title());

        // Get Post Thumbnail for pinterest
        //$sb_thumb = get_the_post_thumbnail_src(get_the_post_thumbnail());
        $sb_thumb = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));

        $cmsmasters_post_image_link = get_post_meta(get_the_ID(), 'cmsmasters_post_image_link', true);

        if (get_post_format() == 'image' && $cmsmasters_post_image_link != '') {
            $pinterest_img = explode('|', $cmsmasters_post_image_link);

            $pinterest_img = $pinterest_img[1];
        } elseif (has_post_thumbnail()) {
            $post_img_id = get_post_thumbnail_id();
            $post_img_id = get_post_thumbnail_id();

            $post_img_url = wp_get_attachment_url($post_img_id);

            $pinterest_img = urlencode($post_img_url);
        } else {
            preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', do_shortcode(get_the_content()), $img_matches);


            if (!empty($img_matches[1][0])) {
                $first_img = $img_matches[1][0];
            } else {
                $options_general = get_option('cmsmasters_options_' . CMSMASTERS_ACTIVE_THEME . '_general');
                $logo_img = $options_general[CMSMASTERS_ACTIVE_THEME . '_logo_url'];

                if($logo_img != '') {
                    $first_img = explode('|', $logo_img);
                    $first_img = $first_img[1];
                } else {
                    $first_img = get_template_directory_uri() . '/theme-vars/theme-style' . CMSMASTERS_CONTENT_COMPOSER_THEME_STYLE . '/img/logo.png';
                }
            }


            $pinterest_img = urlencode($first_img);
        }







        // Construct sharing URL without using any script
        $twitterURL = 'https://twitter.com/intent/tweet?text='.$sb_title.'&amp;url='.$sb_url;
        $facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$sb_url;
        $bufferURL = 'https://bufferapp.com/add?url='.$sb_url.'&amp;text='.$sb_title;
        $whatsappURL = 'whatsapp://send?text='.$sb_title . ' ' . $sb_url;
        $linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$sb_url.'&amp;title='.$sb_title;

        if(!empty($sb_thumb)) {
            $pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$sb_url.'&amp;data-media='.$sb_thumb.'&amp;description='.$sb_title;
        }
        else {
            $pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$sb_url.'&amp;description='.$sb_title;
        }

        // Based on popular demand added Pinterest too
        $pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$sb_url.'&amp;media='.$pinterest_img.'&amp;description='.$sb_title;

        //$gplusURL ='https://plus.google.com/share?url='.$sb_url.'';
        if($arr->is_sticky){
            $_fb_class="share facebook";
            $_twitter_class="share twitter";
            //$_google_class="share google";
            $_linkedin_class="share linkedin";
            $_pinterest_class="share pinterest";
        }else{
            // Add sharing button at the end of page/page content
            $content .= '<div class="share-box"><p class="title">';
            $_fb_class="share facebook img-circle";
            $_twitter_class="share twitter img-circle";
            //$_google_class="share google img-circle";
            $_linkedin_class="share linkedin img-circle";
            $_pinterest_class="share pinterest img-circle";
        }
        $content .= '<a class="'.$_fb_class.'" href="'.$facebookURL.'" target="_blank" rel="nofollow"><i class="fa icon-facebook"></i></a>';
        //$content .= '<a class="'.$_fb_class.'" href="'.$facebookURL.'" target="_blank" rel="nofollow"><img src="'.get_template_directory_uri().'/theme-framework/theme-style/img/facebook.png" /></a>';
        //$content .= '<a class="'.$_twitter_class.'" href="'. $twitterURL .'" target="_blank" rel="nofollow"><img src="'.get_template_directory_uri().'/theme-framework/theme-style/img/twitter.png" /></a>';
        $content .= '<a class="'.$_twitter_class.'" href="'. $twitterURL .'" target="_blank" rel="nofollow"><i class="fa icon-twitter"></i></a>';
        //$content .= '<a class="'.$_google_class.'" href="'.$gplusURL.'" target="_blank" rel="nofollow"><i class="fa fa-google-plus"></i></a>';
        $content .= '<a class="'.$_linkedin_class.'" href="'.$linkedInURL.'" target="_blank" rel="nofollow"><i class="fa icon-linkedin"></i></a>';
        //$content .= '<a class="'.$_pinterest_class.'" href="'.$pinterestURL.'" data-pin-custom="true" target="_blank" rel="nofollow"><img src="'.get_template_directory_uri().'/theme-framework/theme-style/img/pinterest.png" /></a>';
        $content .= '<a class="'.$_pinterest_class.'" href="'.$pinterestURL.'" data-pin-custom="true" target="_blank" rel="nofollow"><i class="fa icon-pinterest"></i></a>';
        //$content .= '<a class="col-2 sbtn s-whatsapp" href="'.$whatsappURL.'" target="_blank" rel="nofollow"><span>WhatsApp</span></a>';
        //$content .= '<a class="col-2 sbtn s-buffer" href="'.$bufferURL.'" target="_blank" rel="nofollow"><span>Buffer</span></a>';
        if($arr->is_sticky==false){
            $content .= '</p></div>';
        }
        return $content;
    }else{
        // if not a post/page then don't include sharing button
        return $content;
    }
}

add_shortcode('mbh_social_buttons','mbh_social_buttons');




/* Get About Author Box Function */
function blogosphere_author_box($title_box = false, $tag = 'h3', $author_tag = 'h4') {
	$user_email = get_the_author_meta('user_email');
	
	
	$user_first_name = get_the_author_meta('first_name') ? get_the_author_meta('first_name') : false;
	
	$user_last_name = get_the_author_meta('last_name') ? get_the_author_meta('last_name') : false;
	
	$user_url = get_the_author_meta('url') ? get_the_author_meta('url') : false;
	
	$user_description = get_the_author_meta('description') ? get_the_author_meta('description') : false;
	
	
	if ($user_description) {
		echo '<aside class="about_author">';
		
		
		if ($title_box) {
			echo '<div class="cmsmasters_about_author_title_wrap"><' . esc_html($tag) . ' class="about_author_title">' . esc_html($title_box) . '</' . esc_html($tag) . '></div>';
		}
		
		
		echo '<div class="about_author_inner">';
		
		
		$out = '';
		
		
		if ($user_first_name) {
			$out .= $user_first_name;
		}
		
		
		if ($user_first_name && $user_last_name) {
			$out .= ' ' . $user_last_name;
		} elseif ($user_last_name) {
			$out .= $user_last_name;
		}
		
		
		if (get_the_author() && ($user_first_name || $user_last_name)) {
			$out .= ' (';
		}
		
		
		if (get_the_author()) {
			$out .= get_the_author();
		}
		
		
		if (get_the_author() && ($user_first_name || $user_last_name)) {
			$out .= ')';
		}
		
		
		echo '<figure class="about_author_avatar">' . 
			get_avatar($user_email, 80, get_option('avatar_default')) . 
		'</figure>' . 
		'<div class="about_author_cont">';
		
		
		if ($user_url) {
			echo '<a href="' . esc_url($user_url) . '" class="cmsmasters_author_website" title="' . esc_attr(get_the_author()) . ' ' . esc_attr__('website', 'blogosphere') . '" target="_blank">' . str_replace(array('http://','https://'), '', esc_url($user_url)) . '</a>';
		}
		
		
		if ($out != '') {
			echo '<' . esc_html($author_tag) . ' class="about_author_cont_title vcard author"><span class="fn" rel="author">' . esc_html($out) . '</span></' . esc_html($author_tag) . '>';
		}
		
		
		echo '<p>' . str_replace("\n", '<br />', $user_description) . '</p>';
		
		
		echo '</div>' . 
			'</div>' . 
		'</aside>';
	}
}



/* Get Related, Popular & Recent Posts Function */
function blogosphere_related($tag = 'h3', $title = '', $no_title = '', $box_type = false, $tgsarray = null, $items_number = 5, $pause_time = 5, $type = 'post', $taxonomy = null) {
	if ( 
		($box_type == 'related' && !empty($tgsarray)) || 
		$box_type == 'popular' || 
		$box_type == 'recent' 
	) {
		$autoplay = ((int) $pause_time > 0) ? $pause_time * 1000 : 'false';
		
		
		if ($type == 'post') {
			$cmsmasters_image_thumb_size = 'cmsmasters-post-slider-thumb';
		} else {
			$cmsmasters_image_thumb_size = 'cmsmasters-project-thumb';
		}
		
		
		$r_args = array( 
			'posts_per_page' => $items_number, 
			'post_status' => 'publish', 
			'ignore_sticky_posts' => 1, 
			'post__not_in' => array(get_the_ID()), 
			'post_type' => $type 
		);
		
		
		if ($box_type == 'related' && !empty($tgsarray)) {
			if ($type == 'post') {
				$r_args['tag__in'] = $tgsarray;
			} elseif ($type != 'post' && $taxonomy) {
				$r_args['tax_query'] = array( 
					array( 
						'taxonomy' => $taxonomy, 
						'field' => 'term_id', 
						'terms' => $tgsarray 
					) 
				);
			}
		} elseif ($box_type == 'popular') {
			$r_args['order'] = 'DESC';
			
			$r_args['orderby'] = 'meta_value_num';
			
			$r_args['meta_key'] = 'cmsmasters_likes';
		}
		
		
		$r_query = new WP_Query($r_args);
		
		
		if ($r_query->have_posts()) {
			echo "<aside class=\"cmsmasters_single_slider\">" . 
				"<div class=\"cmsmasters_single_slider_title_wrap\"><" . esc_html($tag) . " class=\"cmsmasters_single_slider_title\">" . 
					($title != '' ? esc_html($title) : esc_html__('More items', 'blogosphere')) . 
				"</" . esc_html($tag) . "></div>" . 
				'<div class="cmsmasters_single_slider_inner">' . 
					'<div' . 
						' id="cmsmasters_owl_slider_' . esc_attr(uniqid()) . '"' . 
						' class="cmsmasters_owl_slider"' . 
						' data-single-item="false"' . 
						' data-auto-play="' . esc_attr($autoplay) . '"' . 
					'>';
						
						while ($r_query->have_posts()) : $r_query->the_post();
							echo "<div class=\"cmsmasters_owl_slider_item cmsmasters_single_slider_item\">
								<div class=\"cmsmasters_single_slider_item_outer\">";
								
									blogosphere_thumb(get_the_ID(), $cmsmasters_image_thumb_size, true, false, true, false, true, true, false, false, false, 'cmsmasters_theme_icon_image');
									
									echo "<div class=\"cmsmasters_single_slider_item_inner\">";
										
										blogosphere_get_post_date('post');
										
										echo "<h4 class=\"cmsmasters_single_slider_item_title\">
											<a href=\"" . esc_url(get_permalink()) . "\">" . cmsmasters_title(get_the_ID(), false) . "</a>
										</h4>
									</div>
								</div>
							</div>";
						endwhile;
						
					echo "</div>
				</div>
			</aside>";
		}
		
		
		wp_reset_postdata();
	}
}



/* Get Posts Author Avatar Function */
function blogosphere_author_avatar($template_type = 'page') {
	$user_email = get_the_author_meta('user_email') ? get_the_author_meta('user_email') : false;
	
	
	if ($template_type == 'page') {
		if (get_the_tags()) {
			echo '<figure>' . 
				get_avatar($user_email, 75, get_option('avatar_default')) . 
			'</figure>';
		}
	} else if ($template_type == 'post') {
		if (get_the_tags()) {
			echo '<figure>' . 
				get_avatar($user_email, 75, get_option('avatar_default')) . 
			'</figure>';
		}
	}
}



/* Get Pingbacks & Trackbacks Function */
function blogosphere_get_pings($id, $tag = 'h3') {
	$out = '';
	
	$pings = get_comments(array(
		'type' => 		'pings',
		'post_id' => 	$id
	));
	
	
	if (sizeof($pings) > 0) {
		$out .= '<aside class="cmsmasters_pings_list">' . "\n" .
			'<' . esc_html($tag) . '>' . esc_html__('Trackbacks and Pingbacks', 'blogosphere') . '</' . esc_html($tag) . '>' . "\n" .
			'<div class="cmsmasters_pings_wrap">' . "\n" .
				'<ol class="pingslist">' . "\n";
		
		
		$out .= wp_list_comments(array(
			'short_ping' => 	true,
			'echo' => 			false
		), $pings);
		
		
		$out .= '</ol>' . "\n" .
			'</div>' . "\n" .
		'</aside>';
	}
	
	
	return $out;
}

