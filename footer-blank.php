
<!-- subscription Modal -->
<div class="modal fade subscriptionModal" id="subscriptionModal" tabindex="-1" role="dialog" aria-labelledby="subscriptionModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <div class="modal-body-wrap">
                    <h5 class="modal-title" id="subscriptionModalLabel">Access our blogs</h5>
                    <div class="text">
                        <p>Our blogs have all you want to know about muslim marriage.</p>
                    </div>
                    <div class="subscription-form">
                        <?php echo do_shortcode('[mc4wp_form id="14311"]'); ?>
                        <!--<div class="form-group">
                            <input type="email" class="form-control" placeholder="Email address">
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="privacy">
                            <label class="custom-control-label" for="privacy">
                                By continuing, you agree to <a href="/privacy-policy">MuslimBetterHalf’s Privacy Policy.</a>
                            </label>
                        </div>
                        <button type="submit" class="btn">Enjoy Reading</button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php wp_footer(); ?>
<script>
    jQuery(document).ready(function(){
        jQuery('.custom-menu-subscription a').attr('data-toggle','modal');
    });

</script>

</body>
</html>

