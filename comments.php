<?php
/**
 * @package 	WordPress
 * @subpackage 	Blogosphere
 * @version		1.0.4
 * 
 * Comments Template
 * Created by CMSMasters
 * 
 */


if (post_password_required()) { 
	echo '<p class="nocomments">' . esc_html__('This post is password protected. Enter the password to view comments.', 'blogosphere') . '</p>';
	
	
    return;
}


if (have_comments()) {
	echo '<aside id="comments" class="post_comments">' . "\n" . 
		'<h2 class="post_comments_title">';
	
	
	comments_number(esc_attr__('No Comments', 'blogosphere'), esc_attr__('Comment', 'blogosphere') . ' (1)', esc_attr__('Comments', 'blogosphere') . ' (%)');
	
	
	echo '</h2>' . "\n";
	
	
	if (get_previous_comments_link() || get_next_comments_link()) {
		echo '<aside class="comments_nav">';
			
			if (get_previous_comments_link()) {
				echo '<span class="comments_nav_prev cmsmasters_theme_icon_comments_nav_prev">';
					
					previous_comments_link(esc_attr__('Older Comments', 'blogosphere'));
					
				echo '</span>';
			}
			
			
			if (get_next_comments_link()) {
				echo '<span class="comments_nav_next cmsmasters_theme_icon_comments_nav_next">';
					
					next_comments_link(esc_attr__('Newer Comments', 'blogosphere'));
					
				echo '</span>';
			}
			
		echo '</aside>';
	}
	
	
	echo '<ol class="commentlist">' . "\n";
	
	
	wp_list_comments(array( 
		'type' => 'comment', 
		'callback' => 'blogosphere_mytheme_comment' 
	));
	
	
	echo '</ol>' . "\n";
	
	
	if (get_previous_comments_link() || get_next_comments_link()) {
		echo '<aside class="comments_nav">';
			
			if (get_previous_comments_link()) {
				echo '<span class="comments_nav_prev cmsmasters_theme_icon_comments_nav_prev">';
					
					previous_comments_link(esc_attr__('Older Comments', 'blogosphere'));
					
				echo '</span>';
			}
			
			
			if (get_next_comments_link()) {
				echo '<span class="comments_nav_next cmsmasters_theme_icon_comments_nav_next">';
					
					next_comments_link(esc_attr__('Newer Comments', 'blogosphere'));
					
				echo '</span>';
			}
			
		echo '</aside>';
	}
	
	
	echo '</aside>';
}


if (!comments_open() && get_comments_number() && post_type_supports(get_post_type(), 'comments')) {
	echo '<h5 class="no-comments cmsmasters_comments_closed">' . esc_html__('Comments are closed.', 'blogosphere') . '</h5>';
}


$form_fields =  array( 
	'author' => '<p class="comment-form-author">' . "\n" . 
		'<input type="text" id="author" name="author" value="' . esc_attr($commenter['comment_author']) . '" size="35"' . ((isset($aria_req)) ? $aria_req : '') . ' placeholder="' . esc_attr__('Your name', 'blogosphere') . (($req) ? ' *' : '') . '" />' . "\n" . 
	'</p>' . "\n", 
	'email' => '<p class="comment-form-email">' . "\n" . 
		'<input type="text" id="email" name="email" value="' . esc_attr($commenter['comment_author_email']) . '" size="35"' . ((isset($aria_req)) ? $aria_req : '') . ' placeholder="' . esc_attr__('Your email', 'blogosphere') . (($req) ? ' *' : '') . '" />' . "\n" . 
	'</p>' . "\n" 
);


if (get_option('show_comments_cookies_opt_in') == '1') {
	$form_fields['cookies'] = '<p class="comment-form-cookies-consent">' . "\n" . 
		'<input type="checkbox" id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" value="yes"' . (empty($commenter['comment_author_email']) ? '' : ' checked="checked"') . ' />' . "\n" . 
		'<label for="wp-comment-cookies-consent">' . esc_html__('Save my name, email, and website in this browser for the next time I comment.', 'blogosphere') . '</label>' . "\n" . 
	'</p>' . "\n";
}

$_total_post_comments = (blogosphere_get_post_comments("post"))?blogosphere_get_post_comments("post"):0;



comment_form(array( 
	'fields' => 			apply_filters('comment_form_default_fields', $form_fields), 
	'comment_field' => 		'<p class="comment-form-comment">' . 
								'<textarea name="comment" id="comment" cols="67" rows="2" placeholder="' . esc_attr__('Share your thoughts...', 'blogosphere') . '"></textarea>' .
							'</p>',
    'must_log_in' => 		'<p class="must-log-in">' .
                                esc_html__('You must be', 'blogosphere') .
                                ' <a href="' . esc_url(wp_login_url(apply_filters('the_permalink', get_permalink()))) . '">'
                                . esc_html__('logged in', 'blogosphere') .
                                '</a> '
                                . esc_html__('to post a comment', 'blogosphere') .
                                '.</p>' . "\n",
    'logged_in_as' => 		'<p class="comments_info">' .
								    esc_html__('Comments', 'blogosphere').
                                    '<span class="comments_total"> ( '.$_total_post_comments.' ) </span>'.
							    '</p>' . "\n",
	'comment_notes_before' => 	'<p class="comment-notes">' .
									esc_html__('Your email address will not be published.', 'blogosphere') . 
								'</p>' . "\n", 
	'comment_notes_after' => 	'', 
	'id_form' => 				'commentform', 
	'id_submit' => 				'submit', 
	'title_reply' => 			'',
	'title_reply_to' => 		esc_html__('Leave your comment to', 'blogosphere'), 
	'cancel_reply_link' => 		esc_html__('Cancel Reply', 'blogosphere'), 
	'label_submit' => 			esc_html__('Post Comment', 'blogosphere')
));
//'title_reply' => 			esc_html__('Leave you reply', 'blogosphere'),
/*<p class="logged-in-as">' .
    esc_html__('Logged in assdafsda', 'blogosphere') .
    ' <a href="' . esc_url(admin_url('profile.php')) . '">' .
        $user_identity .
    '</a>. ' .
    '<a class="all" href="' . esc_url(wp_logout_url(apply_filters('the_permalink', get_permalink()))) . '" title="' . esc_attr__('Log out of this account', 'blogosphere') . '">' .
        esc_html__('Log outsdfasdf?', 'blogosphere') .
    '</a>' .
'</p>*/

