/**
 * @package 	WordPress
 * @subpackage 	Blogosphere
 * @version 	1.0.3
 * 
 * Theme Information
 * Created by CMSMasters
 * 
 */

To update your theme please use the files list from the FILE LOGS below and substitute/add the listed files on your server with the same files in the Updates folder.

Important: after you have updated the theme, in your admin panel please proceed to
Theme Settings - Fonts and click "Save" in any tab,
then proceed to 
Theme Settings - Colors and click "Save" in any tab here.


-----------------------------------------------------------------------
				FILE LOGS
-----------------------------------------------------------------------

Version 1.0.4: files operations:

	Theme Files edited:
		blogosphere/comments.php
		blogosphere/framework/admin/inc/js/admin-theme-scripts.js
		blogosphere/framework/admin/options/cmsmasters-theme-options-post.php
		blogosphere/framework/admin/options/cmsmasters-theme-options-project.php
		blogosphere/framework/admin/options/cmsmasters-theme-options.php
		blogosphere/framework/admin/options/js/cmsmasters-theme-options.js
		blogosphere/framework/admin/settings/cmsmasters-theme-settings-general.php
		blogosphere/functions.php
		blogosphere/gutenberg/cmsmasters-framework/theme-style/cmsmasters-module-functions.php
		blogosphere/gutenberg/cmsmasters-framework/theme-style/css/editor-style.css
		blogosphere/gutenberg/cmsmasters-framework/theme-style/css/less/module-style.less
		blogosphere/gutenberg/cmsmasters-framework/theme-style/function/module-colors.php
		blogosphere/gutenberg/cmsmasters-framework/theme-style/function/module-fonts.php
		blogosphere/gutenberg/cmsmasters-framework/theme-style/js/editor-options.js
		blogosphere/page.php
		blogosphere/theme-framework/theme-style/cmsmasters-c-c/shortcodes/cmsmasters-portfolio.php
		blogosphere/theme-framework/theme-style/css/adaptive.css
		blogosphere/theme-framework/theme-style/css/less/adaptive.less
		blogosphere/theme-framework/theme-style/css/less/style.less
		blogosphere/theme-framework/theme-style/css/style.css
		blogosphere/theme-framework/theme-style/postType/portfolio/project-puzzle.php
		blogosphere/theme-vars/plugins/cmsmasters-contact-form-builder.zip
		blogosphere/theme-vars/plugins/cmsmasters-custom-fonts.zip
		blogosphere/theme-vars/plugins/cmsmasters-mega-menu.zip
		blogosphere/theme-vars/plugins/LayerSlider.zip
		blogosphere/theme-vars/plugins/revslider.zip
		blogosphere/theme-vars/theme-style/admin/theme-settings-defaults.php
		blogosphere/woocommerce/cmsmasters-framework/theme-style/cmsmasters-plugin-functions.php
		blogosphere/woocommerce/cmsmasters-framework/theme-style/css/less/plugin-style.less
		blogosphere/woocommerce/cmsmasters-framework/theme-style/css/plugin-style.css
		blogosphere/woocommerce/cmsmasters-framework/theme-style/function/plugin-template-functions.php
		blogosphere/woocommerce/cmsmasters-framework/theme-style/js/jquery.plugin-script.js
		blogosphere/woocommerce/cmsmasters-framework/theme-style/templates/content-product.php
		blogosphere/woocommerce/cmsmasters-framework/theme-style/templates/content-single-product.php
		blogosphere/woocommerce/cmsmasters-framework/theme-style/templates/content-widget-product.php
		blogosphere/woocommerce/cmsmasters-framework/theme-style/templates/single-product-reviews.php
		blogosphere/woocommerce/cmsmasters-framework/theme-style/templates/single-product/product-image.php
		blogosphere/woocommerce/content-product.php
		blogosphere/woocommerce/content-single-product.php
		blogosphere/woocommerce/content-widget-product.php
		blogosphere/woocommerce/single-product-reviews.php
		blogosphere/woocommerce/cmsmasters-framework/theme-style/admin/plugin-settings.php
		
		Theme Files added:
			blogosphere/woocommerce/cmsmasters-framework/theme-style/admin/js/plugin-settings-toggle.js
			blogosphere/woocommerce/cmsmasters-framework/theme-style/admin/plugin-settings.php
			
			Proceed to wp-content\plugins\cmsmasters-contact-form-builder
			and update all files in this folder to version 1.4.7
				
			Proceed to wp-content\plugins\cmsmasters-content-composer
			and update all files in this folder to version 2.3.8
			
			
--------------------------------------			
			

Version 1.0.3: files operations:
	Theme Files edited:
		blogosphere\framework\admin\inc\config-functions.php
		blogosphere\framework\admin\inc\css\admin-theme-styles.css
		blogosphere\framework\admin\options\cmsmasters-theme-options-other.php
		blogosphere\framework\admin\options\cmsmasters-theme-options-page.php
		blogosphere\framework\admin\options\cmsmasters-theme-options-post.php
		blogosphere\framework\admin\options\cmsmasters-theme-options.php
		blogosphere\framework\admin\options\css\cmsmasters-theme-options.css
		blogosphere\framework\admin\options\js\cmsmasters-theme-options.js
		blogosphere\framework\admin\settings\cmsmasters-theme-settings-font.php
		blogosphere\framework\admin\settings\cmsmasters-theme-settings-general.php
		blogosphere\framework\admin\settings\cmsmasters-theme-settings.php
		blogosphere\framework\admin\settings\css\cmsmasters-theme-settings-rtl.css
		blogosphere\framework\admin\settings\css\cmsmasters-theme-settings.css
		blogosphere\framework\admin\settings\inc\cmsmasters-helper-functions.php
		blogosphere\framework\admin\settings\js\cmsmasters-theme-settings.js
		blogosphere\framework\class\class-tgm-plugin-activation.php
		blogosphere\framework\function\general-functions.php
		blogosphere\framework\function\theme-categories-icon.php
		blogosphere\functions.php
		blogosphere\js\jquery.script.js
		blogosphere\js\smooth-sticky.min.js
		blogosphere\search.php
		blogosphere\sidebar.php
		blogosphere\style.css
		blogosphere\theme-framework\theme-style\class\theme-widgets.php
		blogosphere\theme-framework\theme-style\css\less\general.less
		blogosphere\theme-framework\theme-style\css\less\style.less
		blogosphere\theme-framework\theme-style\css\style.css
		blogosphere\theme-framework\theme-style\function\template-functions.php
		blogosphere\theme-framework\theme-style\function\theme-fonts.php
		blogosphere\theme-framework\theme-style\js\jquery.theme-script.js
		blogosphere\theme-framework\theme-style\theme-functions.php
		blogosphere\theme-vars\plugin-activator.php
		blogosphere\theme-vars\theme-style\admin\theme-settings-defaults.php
		blogosphere\woocommerce\cmsmasters-framework\theme-style\css\less\plugin-style.less
		blogosphere\woocommerce\cmsmasters-framework\theme-style\css\plugin-style.css
		blogosphere\woocommerce\cmsmasters-framework\theme-style\function\plugin-template-functions.php
		blogosphere\woocommerce\cmsmasters-framework\theme-style\js\jquery.plugin-script.js
		blogosphere\woocommerce\cmsmasters-framework\theme-style\templates\content-single-product.php
		blogosphere\woocommerce\cmsmasters-framework\theme-style\templates\content-widget-product.php
		blogosphere\woocommerce\cmsmasters-framework\theme-style\templates\single-product-reviews.php
		blogosphere\woocommerce\cmsmasters-framework\theme-style\templates\single-product\product-image.php
		blogosphere\woocommerce\cmsmasters-framework\theme-style\templates\single-product\product-thumbnails.php
		blogosphere\woocommerce\content-widget-product.php
		blogosphere\woocommerce\single-product-reviews.php
		blogosphere\woocommerce\single-product\product-image.php
		blogosphere\woocommerce\single-product\product-thumbnails.php
		blogosphere\woocommerce\cmsmasters-framework\theme-style\css\less\plugin-style.less
		blogosphere\woocommerce\cmsmasters-framework\theme-style\css\plugin-style.css
		blogosphere\readme.txt

	Theme Files added:
		blogosphere\gutenberg\cmsmasters-framework\theme-style\cmsmasters-module-functions.php
		blogosphere\gutenberg\cmsmasters-framework\theme-style\css\editor-style.css
		blogosphere\gutenberg\cmsmasters-framework\theme-style\css\frontend-style.css
		blogosphere\gutenberg\cmsmasters-framework\theme-style\css\less\editor-style.less
		blogosphere\gutenberg\cmsmasters-framework\theme-style\css\less\frontend-style.less
		blogosphere\gutenberg\cmsmasters-framework\theme-style\css\less\module-style.less
		blogosphere\gutenberg\cmsmasters-framework\theme-style\css\module-rtl.css
		blogosphere\gutenberg\cmsmasters-framework\theme-style\function\module-colors.php
		blogosphere\gutenberg\cmsmasters-framework\theme-style\function\module-fonts.php
		blogosphere\gutenberg\cmsmasters-framework\theme-style\js\editor-options.js
		blogosphere\theme-vars\plugins\cmsmasters-custom-fonts.zip
		blogosphere\theme-vars\plugins\cmsmasters-importer.zip
		blogosphere\theme-vars\theme-style\admin\demo-content-importer.php
		blogosphere\theme-vars\theme-style\admin\demo-content\main\content.xml
		blogosphere\theme-vars\theme-style\admin\demo-content\main\theme-settings.txt
		blogosphere\theme-vars\theme-style\admin\demo-content\main\widgets.json
	
	Proceed to wp-content\plugins\cmsmasters-content-composer 
	and update all files in this folder to version 2.3.5

	Proceed to wp-content\plugins\cmsmasters-contact-form-builder
	and update all files in this folder to version 1.4.4

	Proceed to wp-content\plugins\cmsmasters-mega-menu
	and update all files in this folder to version 1.2.9

	Proceed to wp-content\plugins\cmsmasters-importer
	and update all files in this folder to version 1.0.3

	Proceed to wp-content\plugins\cmsmasters-custom-fonts
	and update all files in this folder to version 1.0.1

	Proceed to wp-content\plugins\revslider
	and update all files in this folder to version 5.4.8.1



--------------------------------------


Version 1.0.2: files operations:

	Theme Files edited:
		blogosphere\archive.php
		blogosphere\author.php
		blogosphere\footer.php
		blogosphere\framework\admin\options\cmsmasters-theme-options.php
		blogosphere\framework\admin\settings\cmsmasters-theme-settings.php
		blogosphere\framework\function\general-functions.php
		blogosphere\framework\function\likes.php
		blogosphere\framework\function\views.php
		blogosphere\functions.php
		blogosphere\header.php
		blogosphere\image.php
		blogosphere\index.php
		blogosphere\js\smooth-sticky.min.js
		blogosphere\page.php
		blogosphere\readme.txt
		blogosphere\search.php
		blogosphere\sidebar-bottom.php
		blogosphere\sidebar.php
		blogosphere\single-profile.php
		blogosphere\single-project.php
		blogosphere\single.php
		blogosphere\sitemap.php
		blogosphere\style.css
		blogosphere\theme-framework\theme-style\css\adaptive.css
		blogosphere\theme-framework\theme-style\css\less\adaptive.less
		blogosphere\theme-framework\theme-style\css\less\style.less
		blogosphere\theme-framework\theme-style\css\style.css
		blogosphere\theme-framework\theme-style\function\template-functions-post.php
		blogosphere\theme-framework\theme-style\function\template-functions-profile.php
		blogosphere\theme-framework\theme-style\function\template-functions-project.php
		blogosphere\theme-framework\theme-style\function\template-functions-shortcodes.php
		blogosphere\theme-framework\theme-style\function\template-functions.php
		blogosphere\theme-framework\theme-style\function\theme-colors-primary.php
		blogosphere\theme-framework\theme-style\function\theme-fonts.php
		blogosphere\theme-framework\theme-style\js\jquery.isotope.mode.js
		blogosphere\theme-framework\theme-style\postType\blog\post-default.php
		blogosphere\theme-framework\theme-style\postType\blog\post-masonry.php
		blogosphere\theme-framework\theme-style\postType\blog\post-puzzle.php
		blogosphere\theme-framework\theme-style\postType\blog\post-side.php
		blogosphere\theme-framework\theme-style\postType\blog\post-single.php
		blogosphere\theme-framework\theme-style\postType\blog\post-timeline.php
		blogosphere\theme-framework\theme-style\postType\portfolio\project-grid.php
		blogosphere\theme-framework\theme-style\postType\portfolio\project-puzzle.php
		blogosphere\theme-framework\theme-style\postType\portfolio\project-single.php
		blogosphere\theme-framework\theme-style\postType\posts-slider\slider-post.php
		blogosphere\theme-framework\theme-style\postType\posts-slider\slider-project.php
		blogosphere\theme-framework\theme-style\postType\profile\profile-horizontal.php
		blogosphere\theme-framework\theme-style\postType\profile\profile-single.php
		blogosphere\theme-framework\theme-style\postType\profile\profile-vertical.php
		blogosphere\theme-framework\theme-style\postType\quote\quote-grid.php
		blogosphere\theme-framework\theme-style\postType\quote\quote-slider.php
		blogosphere\theme-framework\theme-style\template\404.php
		blogosphere\theme-framework\theme-style\template\header-bot.php
		blogosphere\theme-framework\theme-style\template\header-mid.php
		blogosphere\theme-vars\languages\blogosphere.pot
		blogosphere\theme-vars\plugin-activator.php
		blogosphere\theme-vars\plugins\cmsmasters-contact-form-builder.zip
		blogosphere\theme-vars\plugins\cmsmasters-content-composer.zip
		blogosphere\theme-vars\plugins\cmsmasters-mega-menu.zip
		blogosphere\theme-vars\plugins\LayerSlider.zip
		blogosphere\theme-vars\plugins\revslider.zip
		blogosphere\theme-vars\theme-style\css\styles\blogosphere.css
		blogosphere\woocommerce\cmsmasters-framework\theme-style\css\less\plugin-style.less
		blogosphere\woocommerce\cmsmasters-framework\theme-style\css\plugin-style.css
		blogosphere\woocommerce\cmsmasters-framework\theme-style\function\plugin-template-functions.php
		blogosphere\woocommerce\cmsmasters-framework\theme-style\templates\content-widget-product.php
		blogosphere\woocommerce\cmsmasters-framework\theme-style\templates\global\sidebar.php
		blogosphere\woocommerce\cmsmasters-framework\theme-style\templates\global\wrapper-end.php
		blogosphere\woocommerce\cmsmasters-framework\theme-style\templates\global\wrapper-start.php
		blogosphere\woocommerce\cmsmasters-framework\theme-style\templates\single-product\meta.php
		
		
	Theme Files removed:
		blogosphere\js\query-loader.min.js
		blogosphere\theme-vars\plugins\envato-market.zip
		
		
	Proceed to wp-content\plugins\cmsmasters-content-composer
	and update all files in this folder to version 2.2.8
	
	Proceed to wp-content\plugins\revslider
	and update all files in this folder to version 5.4.8



--------------------------------------


Version 1.0.1: files operations:

	Theme Files edited:
		blogosphere\comments.php
		blogosphere\framework\admin\inc\admin-scripts.php
		blogosphere\framework\function\general-functions.php
		blogosphere\functions.php
		blogosphere\js\jquery.script.js
		blogosphere\js\smooth-sticky.min.js
		blogosphere\style.css
		blogosphere\theme-framework\theme-style\admin\theme-settings.php
		blogosphere\theme-framework\theme-style\cmsmasters-c-c\cmsmasters-c-c-theme-functions.php
		blogosphere\theme-framework\theme-style\cmsmasters-c-c\js\cmsmasters-c-c-theme-extend.js
		blogosphere\theme-framework\theme-style\css\adaptive.css
		blogosphere\theme-framework\theme-style\css\less\adaptive.less
		blogosphere\theme-framework\theme-style\css\less\style.less
		blogosphere\theme-framework\theme-style\css\rtl.css
		blogosphere\theme-framework\theme-style\css\style.css
		blogosphere\theme-framework\theme-style\function\template-functions-post.php
		blogosphere\theme-framework\theme-style\function\template-functions-project.php
		blogosphere\theme-framework\theme-style\function\template-functions-shortcodes.php
		blogosphere\theme-framework\theme-style\function\template-functions-single.php
		blogosphere\theme-framework\theme-style\function\template-functions.php
		blogosphere\theme-framework\theme-style\function\theme-colors-primary.php
		blogosphere\theme-framework\theme-style\function\theme-fonts.php
		blogosphere\theme-framework\theme-style\template\footer.php
		blogosphere\theme-framework\theme-style\postType\blog\post-default.php
		blogosphere\theme-framework\theme-style\postType\blog\post-masonry.php
		blogosphere\theme-framework\theme-style\postType\blog\post-puzzle.php
		blogosphere\theme-framework\theme-style\postType\blog\post-side.php
		blogosphere\theme-framework\theme-style\postType\blog\post-single.php
		blogosphere\theme-framework\theme-style\postType\blog\post-timeline.php
		blogosphere\theme-framework\theme-style\postType\portfolio\project-grid.php
		blogosphere\theme-framework\theme-style\postType\portfolio\project-puzzle.php
		blogosphere\theme-framework\theme-style\postType\portfolio\project-single.php
		blogosphere\theme-framework\theme-style\postType\posts-slider\slider-post.php
		blogosphere\theme-framework\theme-style\postType\posts-slider\slider-project.php
		blogosphere\theme-framework\theme-style\postType\profile\profile-single.php
		blogosphere\theme-vars\languages\blogosphere.pot
		blogosphere\theme-vars\plugin-activator.php
		blogosphere\theme-vars\theme-style\admin\theme-settings-defaults.php
		blogosphere\theme-vars\theme-style\css\styles\blogosphere.css
		blogosphere\theme-vars\theme-style\css\vars-style.css
		blogosphere\theme-vars\theme-style\css\vars-style.less
		blogosphere\theme-vars\theme-style\theme-vars-functions.php
		blogosphere\woocommerce\archive-product.php
		blogosphere\woocommerce\cmsmasters-framework\theme-style\css\less\plugin-style.less
		blogosphere\woocommerce\cmsmasters-framework\theme-style\css\plugin-style.css
		blogosphere\woocommerce\cmsmasters-framework\theme-style\templates\archive-product.php
		blogosphere\woocommerce\cmsmasters-framework\theme-style\templates\content-product.php
		blogosphere\woocommerce\cmsmasters-framework\theme-style\templates\content-single-product.php
		blogosphere\woocommerce\cmsmasters-framework\theme-style\templates\single-product-reviews.php
		blogosphere\woocommerce\content-product.php
		blogosphere\woocommerce\content-single-product.php
		
		
	Theme Files added:
		blogosphere\js\smooth-sticky.min.js
		
		
	Proceed to wp-content\plugins\cmsmasters-contact-form-builder
	and update all files in this folder to version 1.4.3
	
	Proceed to wp-content\plugins\cmsmasters-content-composer
	and update all files in this folder to version 2.2.4
	
	Proceed to wp-content\plugins\LayerSlider
	and update all files in this folder to version 6.7.6
	
	Proceed to wp-content\plugins\revslider
	and update all files in this folder to version 5.4.7.4



--------------------------------------


Version 1.0: Release!

