<?php
/**
 * @package 	WordPress
 * @subpackage 	Blogosphere
 * @version		1.0.4
 * 
 * Default Page Template
 * Created by CMSMasters
 * 
 */

global $post;

if ( is_front_page() && is_home() ) {

    get_header();
} elseif ( is_front_page()){
    get_header();

} elseif ( is_home()){

// Blog page

} else {

    get_header('single');

}

/*if($post->post_name == 'successful-stories' ||$post->post_name == 'islam-marriage' || $post->post_name == 'privacy-policy' || $post->post_name == 'jobs' || $post->post_name == 'contact' || $post->post_name == 'cookie-policy' || $post->post_name == 'safety-tips' || $post->post_name == 'security' ){
    get_header('single');
}else{
    get_header();
}*/


list($cmsmasters_layout) = blogosphere_theme_page_layout_scheme();

echo '<!-- Start Content -->' . "\n";


if ($cmsmasters_layout == 'r_sidebar') {
	echo '<div class="content entry">' . "\n\t";
} elseif ($cmsmasters_layout == 'l_sidebar') {
	echo '<div class="content entry fr">' . "\n\t";
} else {
	echo '<div class="middle_content entry">';
}


if (have_posts()) : the_post();

	$content_start = substr(ltrim(get_post_field('post_content', get_the_ID())), 0, 15);
	
	
	if ($cmsmasters_layout == 'fullwidth' && $content_start === '[cmsmasters_row') {
		echo '</div>' . 
		'</div>';
	}
    /*echo '<div class="mbh-cat-wrap"><div class="cmsmasters_divider solid"></div>';
    echo '<h1>' . esc_html__('Blog Archives by Categories', 'blogosphere') . '</h1>';
    echo '<ul class="cmsmasters_sitemap_category">';;
    wp_list_categories(array(
        'title_li' => '',
        'orderby' => 'name',
        'order' => 'ASC'
    ));
    echo '</div></ul>';*/
	//die(__FILE__);
    //echo apply_filters('get_category_content',the_content());
    /*add_filter('the_content', function ($content) {

        $doc = new Document();
        $doc->html($content);
        echo "<pre>";
        var_dump($doc);
        echo "</pre>";
        die(__FILE__);

        $nodes = $doc->find('#home > .container');

        if (!$nodes->count()) {
            return $content;
        }
        $homeContent = $nodes->eq(0)->html();
        $nodes->eq(0)->html($html . $homeContent);

        return $doc->saveHTML($doc);

        //return $this->injectContentAtHomeTop($content, $this->getHTML());
    }, 10);*/

    /*add_filter("the_content",function($content){
        $doc = new Document();
        $doc->html($content);
        $nodes = $doc->find('#home > .cmsmasters_row_outer_parent');

        return "the content filter";
    }, 10);*/

    the_content();

	echo '<div class="cl"></div>';
	
	
	if ($cmsmasters_layout == 'fullwidth' && $content_start === '[cmsmasters_row') {
		echo '<div class="content_wrap ' . $cmsmasters_layout . '">' . "\n\n" . 
			'<div class="middle_content entry">';
	}
	
	
	wp_link_pages(array( 
		'before' => '<div class="subpage_nav">' . '<strong>' . esc_html__('Pages', 'blogosphere') . ':</strong>', 
		'after' => '</div>' . "\n", 
		'link_before' => '<span>', 
		'link_after' => '</span>' 
	));
	
	
	comments_template();
endif;


echo '</div>' . "\n" . 
'<!-- Finish Content -->' . "\n\n";


if ($cmsmasters_layout == 'r_sidebar') {
	echo "\n" . '<!-- Start Sidebar -->' . "\n" . 
	'<div class="sidebar">' . "\n";
	
	get_sidebar();
	
	echo "\n" . '</div>' . "\n" . 
	'<!-- Finish Sidebar -->' . "\n";
} elseif ($cmsmasters_layout == 'l_sidebar') {
	echo "\n" . '<!-- Start Sidebar -->' . "\n" . 
	'<div class="sidebar fl">' . "\n";
	
	get_sidebar();
	
	echo "\n" . '</div>' . "\n" . 
	'<!-- Finish Sidebar -->' . "\n";
}

if($post->post_name == 'jobs' || $post->post_name == 'contact' ){
    get_footer("blank");
}else{
    get_footer();
}




